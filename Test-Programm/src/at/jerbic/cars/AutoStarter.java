package at.jerbic.cars;
public class AutoStarter {

	public static void main(String[] args) {
		
		Producer d1 = new Producer("de", "audi", 0.3);
		
		Auto a1 = new Auto(200, 30000, "red", 100, null, d1);
		
		Person p1 = new Person("Klaus", "Blume", 120, d1);
		
		System.out.println(a1.getPrice());

	}

}
