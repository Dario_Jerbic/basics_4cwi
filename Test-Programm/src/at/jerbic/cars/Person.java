package at.jerbic.cars;

public class Person {

	private String fName;
	private String sName;
	private int birthday;
	private Producer producer;
	
	public Person(String fName, String sName, int birthday, Producer producer) {
		super();
		this.fName = fName;
		this.sName = sName;
		this.birthday = birthday;
		this.producer = producer;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}

	public Producer getProducer() {
		this.producer.getName();
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	} 
	
}
