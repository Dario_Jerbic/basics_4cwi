package at.jerbic.cars;

public class Producer {

	private String origin;
	private String name;
	private double discount;
	
	public Producer(String origin, String name, double discount) {
		super();
		this.origin = origin;
		this.name = name;
		this.discount = discount;
	}
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	
}
