package at.jerbic.cars;

public class Motor {

	private int fuel;
	private int performance;
	
	public Motor(int fuel, int performance) {
		this.fuel = fuel;
		this.performance = performance;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}

}

