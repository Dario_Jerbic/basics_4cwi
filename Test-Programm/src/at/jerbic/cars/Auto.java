package at.jerbic.cars;

import java.util.ArrayList;
import java.util.List;

public class Auto {

	private int maxspeed;
	private int price;
	private String colour;
	private int ussage;
	private Motor engine;
	private Producer producer;
	private List<Auto> cars;

	public Auto(int maxspeed, int price, String colour, int ussage, Motor engine, Producer producer) {
		super();
		this.maxspeed = maxspeed;
		this.price = price;
		this.colour = colour;
		this.ussage = ussage;
		this.engine = engine;
		this.producer = producer;
		this.cars = new ArrayList<>();
	}

	public int getMaxspeed() {
		return maxspeed;
	}


	public void setMaxspeed(int maxspeed) {
		this.maxspeed = maxspeed;
	}


	public int getPrice() {
		return (int) (price * (1 - producer.getDiscount()));
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public String getColour() {
		return colour;
	}


	public void setColour(String colour) {
		this.colour = colour;
	}


	public int getUssage() {
		return ussage;
	}


	public void setUssage(int ussage) {
		this.ussage = ussage;
	}


	public List<Auto> getCars() {
		return cars;
	}


	public void setCars(List<Auto> cars) {
		this.cars = cars;
	}


	public Motor getEngine() {
		this.engine.getFuel();
		this.engine.getPerformance();
		return engine;
	}

	public Producer getProduce() {
		this.producer.getName();
		this.producer.getOrigin();
		this.producer.getDiscount();
		return producer;
	}
	
	public double getValueOfCars() {
		double result = 0;
		for (Auto car : cars) {
			result += car.getPrice();
		}
		return result;
	} 
	
		
}
