package at.jerbic.remote;

import java.util.ArrayList;
import java.util.List;

// strg  + Leertaste merken = sehr wichtig

public class Remote {

	// private = Gedächtnisvariable
	private int length;
	private int width;
	private int height;
	private int weight; // in g
	private String SerialNumber;
	//private boolean isOn = false;
	private List<Device> devices;

	
	public Remote(int length, int width, int height, int weight, String serialNumber) { //beim constructor beachten den haken bei devices rauszumachen
		super();
		this.length = length;
		this.width = width;
		this.height = height;
		this.weight = weight;
		SerialNumber = serialNumber;
		//this.isOn = isOn;
		this.devices = new ArrayList<>();		//neue Liste wird erstellt
	}

	public void addDevices (Device device) {			//funktion 
		this.devices.add(device);						// bei dieser funktion werden die geräte hinzugefügt
	}
	
	public int getLength() {
		return length;
	}



	public void setLength(int length) {
		this.length = length;
	}



	public int getWidth() {
		return width;
	}



	public void setWidth(int width) {
		this.width = width;
	}



	public int getHeight() {
		return height;
	}



	public void setHeight(int height) {
		this.height = height;
	}



	public int getWeight() {
		return weight;
	}



	public void setWeight(int weight) {
		this.weight = weight;
	}



	public String getSerialNumber() {
		return SerialNumber;
	}



	public void setSerialNumber(String serialNumber) {
		SerialNumber = serialNumber;
	}


	public List<Device> getDevices() {
		return devices;
	}



	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}



	public void sayHello() {
		System.out.println("I have the following values: " + this.weight + " gramm; " + this.length + " cm;");
	}
	
	public void turnOn() {
		System.out.println("Ich bin jetzt eingeschaltet");
	}
	
	public void turnOff() {
		System.out.println("Ich bin jetzt ausgeschaltet");
	}

}
