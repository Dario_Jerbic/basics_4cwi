package at.jerbic.remote;

public class Device {

	private String serialNumber;
	private type deviceType;				//hier werden die ger�te gemerkt
	public enum type {						//hier werden die verschiedenen ger�te festgelegt
		television, beamer, rollo
	}
	
	public Device(String serialNumber, type deviceType) {
		super();
		this.serialNumber = serialNumber;
		this.deviceType = deviceType;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public type getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}
	
	
	
	
}
