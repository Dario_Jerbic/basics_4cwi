package at.jerbic.remote;

public class RemoteStarter {

	public static void main(String[] args) {
		
		Device d1 = new Device("123456789", Device.type.beamer);   //neue ger�te erstellt
		Device d2 = new Device("987654321", Device.type.rollo);

		Remote r1 = new Remote(80, 20, 5, 200, "001");
		//Remote r2 = new Remote(60, 10, 20, 320, "002");
		//Remote r3 = r2;

		r1.addDevices(d1);				//zu dem remote werden ger�te hinzugef�gt
		r1.addDevices(d2);
		
		System.out.println(r1.getDevices().get(1).getDeviceType()); 	//hier werden die ger�tetypen ausgegeben die die nummer 1 in der LIste haben
		
		//r1.sayHello();
		//r2.sayHello();
		//r3.sayHello();
		
		//r2.weight = 22; nicht m�glich wenn weight private ist
		
		//r3.sayHello();

	}

}
